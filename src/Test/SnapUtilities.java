package Test;

import java.io.File;
import java.io.IOException;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.google.common.io.Files;

public class SnapUtilities {
	public static void snap(WebDriver driver,String filename) {
		 TakesScreenshot takesnap = (TakesScreenshot) driver;
		 File file = takesnap.getScreenshotAs(OutputType.FILE);
		 System.out.println("Temp File is:"+file.getAbsolutePath());
		 
		 File dest = new File("Snaps\\"+filename);
		 try {
			 Files.move(file, dest);
			 System.out.println("File Copied to:"+dest.getAbsolutePath());
	 		 } catch(IOException e) {
	 			 e.printStackTrace();
	 		 }
		 	}

}
