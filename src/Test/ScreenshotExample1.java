package Test;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import org.testng.annotations.Test;

public class ScreenshotExample1 {
	
	 @Test
	 public void testscreenshot() {
		 System.setProperty("webdriver.chrome.driver","C:\\Users\\User\\Downloads\\chromedriver_win32\\chromedriver.exe");
			WebDriver driver = new ChromeDriver();	
			driver.manage().window().setSize(new Dimension(375,667));
			driver.get("http://automationpractice.com/index.php");
			SnapUtilities.snap(driver,"iphone6.png");
			driver.manage().window().setSize(new Dimension(1024,1366));
			SnapUtilities.snap(driver,"ipadpro.png");
			driver.quit();
	 }
	
		
	 
  
}
